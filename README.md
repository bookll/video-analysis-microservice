# Video Analysis Microservice #

Microservice to get scene annotations using the Google Cloud Video Intelligence API.

http://video-microservice.herokuapp.com

### What is this repository for? ###

* Pass an video src in to a URL via query string to receive a json repsonse containing annotations
* Version 1.0

### How do I get set up? ###

* Clone repo
* Install deps
* npm start

### Params ###

* video [url to remote gcloud video - Currently only gcloud buckets are supported]
* detect [ LABEL_DETECTION | SHOT_CHANGE_DETECTION | SAFE_SEARCH_DETECTION ]

### Example usage ###


```
#!js

axios.get('http://video-microservice.herokuapp.com', {
    params: {
      video: "gs://wammajam/cat.mp4"
    }
  })
  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.log(error);
  });  
```

### Result ###


```
#!json

{
    "annotationResults": [
        {
            "inputUri": "/wammajam/cat.mp4",
            "labelAnnotations": [
                {
                    "description": "Animal",
                    "languageCode": "en-us",
                    "locations": [
                        {
                            "segment": {
                                "startTimeOffset": {
                                    "low": -1,
                                    "high": -1,
                                    "unsigned": false
                                },
                                "endTimeOffset": {
                                    "low": -1,
                                    "high": -1,
                                    "unsigned": false
                                }
                            },
                            "confidence": 0.9809588193893433,
                            "level": 1
                        },
                        {
                            "segment": {
                                "startTimeOffset": {
                                    "low": 0,
                                    "high": 0,
                                    "unsigned": false
                                },
                                "endTimeOffset": {
                                    "low": 14833664,
                                    "high": 0,
                                    "unsigned": false
                                }
                            },
                            "confidence": 0.9809588193893433,
                            "level": 3
                        }
                    ]
                },
                {
                    "description": "Cat",
                    "languageCode": "en-us",
                    "locations": [
                        {
                            "segment": {
                                "startTimeOffset": {
                                    "low": -1,
                                    "high": -1,
                                    "unsigned": false
                                },
                                "endTimeOffset": {
                                    "low": -1,
                                    "high": -1,
                                    "unsigned": false
                                }
                            },
                            "confidence": 0.9850918650627136,
                            "level": 1
                        },
                        {
                            "segment": {
                                "startTimeOffset": {
                                    "low": 0,
                                    "high": 0,
                                    "unsigned": false
                                },
                                "endTimeOffset": {
                                    "low": 14833664,
                                    "high": 0,
                                    "unsigned": false
                                }
                            },
                            "confidence": 0.9850918650627136,
                            "level": 3
                        }
                    ]
                },
            },
            "faceAnnotations": [],
            "shotAnnotations": [],
            "safeSearchAnnotations": [],
            "error": null
        }
    ]
}
```