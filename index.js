require('dotenv').config()

const {
    send
} = require('micro')
const url = require('url')

const Video = require('@google-cloud/video-intelligence');

const videoClient = Video({
    projectId: 'conductive-fold-172614',
    keyFilename: './config.json'
});

module.exports = function(req, res) {

    const path = url.parse(req.url, true);
    const video = path.query.video || "gs://wammajam/cat.mp4";
    const detect = path.query.detect || "LABEL_DETECTION"; // SHOT_CHANGE_DETECTION, SAFE_SEARCH_DETECTION

    const request = {
        "inputUri": video,
        "features": [
            detect
        ]
    }

    videoClient.annotateVideo(request).then(function(responses) {
        const operation = responses[0];
        const initialApiResponse = responses[1];

        // Operation#promise starts polling for the completion of the LRO.
        return operation.promise();
    }).then(function(responses) {

        console.log(JSON.stringify(responses, null, 4))

        const response = responses[0]
        const annotations = responses[0].annotationResults[0].labelAnnotations;

        res.write(JSON.stringify(response, null, 4))

    }).catch(function(err) {
        console.error(err);
    });

}